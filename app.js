'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Service()
/* eslint-disable new-cap */
const rkhLogger = new reekoh.logger('text-sentiment-analysis-service')

let request = require('request')
let isPlainObject = require('lodash.isplainobject')
let isEmpty = require('lodash.isempty')
let get = require('lodash.get')

plugin.on('data', (data) => {
  let startTime = plugin.processStart()
  let options = plugin.config
  if (!isPlainObject(data)) {
    plugin.log(JSON.stringify({
      title: 'Microsoft Text Sentiment Analysis Service',
      message: 'Invalid data received.'
    }))
    plugin.processDone(startTime)
    return plugin.logException(new Error(`Invalid data received. Must be a valid JSON Object. Data: ${data}`))
  }

  if (isEmpty(data) || isEmpty(get(data, 'documents'))) {
    plugin.log(JSON.stringify({
      title: 'Microsoft Text Sentiment Analysis Service',
      message: 'No documents.'
    }))
    plugin.processDone(startTime)
    return plugin.logException(new Error(`Invalid data received. Data must have a documents fields. Data: ${data}`))
  }

  request.post({
    url: `${options.apiEndPoint}/sentiment`,
    headers: {
      'Ocp-Apim-Subscription-Key': options.apiKey,
      'content-type': 'application/json'
    },
    json: data
  }, (err, body) => {
    if (err) {
      plugin.processDone(startTime)
      console.log(err)
      plugin.logException(err)
    }
    plugin.pipe(data, ({ result: body }))
      .then(() => {
        plugin.processDone(startTime)
        plugin.log({
          title: 'Microsoft Text Sentiment Analysis Service',
          data: data,
          result: body
        })
      }).catch((error) => {
        plugin.processDone(startTime)
        plugin.logException(error)
      })
  })
})

plugin.once('ready', () => {
  rkhLogger.info('Microsoft Text Sentiment Analysis Service has been initialized.')
  plugin.log('Microsoft Text Sentiment Analysis Service has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
