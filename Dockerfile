FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/text-sentiment-analysis-service

WORKDIR /home/node/text-sentiment-analysis-service

# RUN npm install pm2@2.6.1 -g

# CMD pm2-docker --json app.yml

CMD ["node", "app.js"]
