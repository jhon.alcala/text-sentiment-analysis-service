'use strict'

const amqp = require('amqplib')

let app = null
let _conn = null
let _channel = null


describe('Nokia UI Connector> Test', () => {
  before('init', function () {
    process.env.ACCOUNT = 'demo.account'
    process.env.INPUT_PIPE = 'demo.pipe.service'
    process.env.BROKER = 'amqp://guest:guest@localhost'
    process.env.OUTPUT_PIPES = 'Op1,Op2'

    amqp.connect(process.env.BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
        _channel = channel
      }).catch((err) => {
        console.log(err)
      })
  })

  after('close connection', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      app = require('../app')
      app.once('init', done)
    })
  })

  describe('#data', () => {
    it('should send data to third party client', function (done) {
      this.timeout(15000)

      let dummyData = { documents: [{
        language: 'en',
        id: '1',
        text: 'Hello world. This is some input text that I love.'
      },
      {
        language: 'fr',
        id: '2',
        text: 'Bonjour tout le monde'
      },
      {
        language: 'es',
        id: '3',
        text: 'La carretera estaba atascada. Había mucho tráfico el día de ayer.'
      }] }
      _channel.sendToQueue('demo.pipe.service', new Buffer(JSON.stringify(dummyData)))

      setTimeout(done, 10000)
    })
  })

})

